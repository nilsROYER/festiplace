<?php

namespace App\Security\Voter;

use App\Entity\Stand;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class StandVoter extends Voter
{
    public const UPLOAD_IMAGE = 'UPLOAD_IMAGE_STAND';
    public const CREATE_STAND = 'CREATE_STAND';
    public const DELETE_STAND = 'DELETE_STAND';
    public const UPDATE_STAND = 'UPDATE_STAND';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        $supportAttributes = in_array($attribute, [self::UPLOAD_IMAGE, self::CREATE_STAND, self::DELETE_STAND, self::UPDATE_STAND]);
        $supportSubject = $subject instanceof Stand;

        return $supportAttributes && $supportSubject;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::UPLOAD_IMAGE:
                if (
                    ($user->getId() == $subject->getId() && $user->getStand() instanceof Stand && $this->security->isGranted('ROLE_STAND_OWNER')) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
            case self::CREATE_STAND:
                if (
                    ($this->security->isGranted('ROLE_STAND_OWNER') && !$user->getStand() instanceof Stand) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
            case self::UPDATE_STAND:
                if (
                    ($this->security->isGranted('ROLE_STAND_OWNER') && $user->getStand() instanceof Stand && $user->getId() == $subject->getId()) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
            case self::DELETE_STAND:
                if (
                    ($user->getId() == $subject->getId() && $this->security->isGranted('ROLE_STAND_OWNER')) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
        }

        return false;
    }
}
