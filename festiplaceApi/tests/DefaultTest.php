<?php

namespace App\Tests;

use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use PHPUnit\Framework\TestCase;

class DefaultTest extends TestCase
{
    public function testSomething(): void
    {
        $two = 1 + 1;
        $this->assertTrue($two === 2);
    }
}
