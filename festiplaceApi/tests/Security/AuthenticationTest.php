<?php

namespace App\Tests\Security;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;

class AuthenticationTest extends ApiTestCase
{
    public function testLogin(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $user = (new User())
            ->setEmail('test@example.com')
            ->setUsername('username')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setPhone('0600000000');

        $user->setPassword(
            $container->get('security.user_password_hasher')->hashPassword($user, 'password')
        );

        $manager = $container->get('doctrine')->getManager();
        $manager->persist($user);
        $manager->flush();

        // retrieve a token
        $response = $client->request('POST', '/api/login', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'test@example.com',
                'password' => 'password',
            ],
        ]);

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $json);
    }
}
