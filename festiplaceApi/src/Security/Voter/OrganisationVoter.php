<?php

namespace App\Security\Voter;

use App\Datas\Roles;
use App\Entity\Organisation;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class OrganisationVoter extends Voter
{
    public const CREATE_ORGANISATION = 'CREATE_ORGANISATION';
    public const UPDATE_ORGANISATION = 'UPDATE_ORGANISATION';
    public const DELETE_ORGANISATION = 'DELETE_ORGANISATION';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::CREATE_ORGANISATION, self::UPDATE_ORGANISATION, self::DELETE_ORGANISATION])
                && $subject instanceof Organisation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User || !$subject instanceof Organisation) {
            return false;
        }

        return match ($attribute) {
            self::CREATE_ORGANISATION => $this->userHaveNotOrganisation($user),
            self::UPDATE_ORGANISATION, self::DELETE_ORGANISATION => $this->userIsOwnerOfOrganisation(user: $user, subject: $subject) || $this->security->isGranted(Roles::ROLE_ADMIN)
        };
    }

    private function userHaveNotOrganisation(User $user): bool
    {
        return $this->security->isGranted(Roles::ROLE_EVENT_CREATOR) && !$user->getOrganisation() instanceof Organisation;
    }

    private function userIsOwnerOfOrganisation(User $user, Organisation $subject): bool
    {
        return $this->security->isGranted(Roles::ROLE_EVENT_CREATOR) && $user->getOrganisation()->getId() == $subject->getId();
    }
}
