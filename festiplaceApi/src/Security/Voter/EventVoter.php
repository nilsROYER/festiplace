<?php

namespace App\Security\Voter;

use App\Entity\Event;
use App\Entity\Organisation;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class EventVoter extends Voter
{
    public const UPLOAD_IMAGE = 'EVENT_UPLOAD_IMAGE';
    public const CREATE_EVENT = 'EVENT_CREATE';
    public const UPDATE_EVENT = 'EVENT_UPDATE';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        $supportAttributes = in_array($attribute, [self::UPLOAD_IMAGE, self::CREATE_EVENT]);
        $supportSubject = $subject instanceof Event;

        return $supportAttributes && $supportSubject;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User */
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        // check conditions and return true to grant permission
        switch ($attribute) {
            case self::UPLOAD_IMAGE:
                if ($this->security->isGranted('ROLE_EVENT_CREATOR') && $this->userIsOwnerEvent($user, $subject)) {
                    return true;
                }
                break;
            case self::CREATE_EVENT:
                if ($this->security->isGranted('ROLE_EVENT_CREATOR') && $this->userHaveOrganisation($user)) {
                    return true;
                }
                break;
            case self::UPDATE_EVENT:
                if ($this->security->isGranted('ROLE_EVENT_CREATOR') && $this->userIsOwnerEvent($user, $subject)) {
                    return true;
                }
                break;
        }

        return false;
    }

    private function userIsOwnerEvent(User $user, Event $event): bool
    {
        return $this->userHaveOrganisation($user) && $user->getOrganisation()->getEvents()->contains($event);
    }

    private function userHaveOrganisation(User $user): bool
    {
        return $user->getOrganisation() instanceof Organisation;
    }
}
