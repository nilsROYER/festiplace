<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\ParkingPlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    operations: [
        new Get(),
        new Put(),
        new Delete(),
        new GetCollection(),
        new Post()
    ],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: ParkingPlaceRepository::class)]
class ParkingPlace
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id;

    #[ORM\Column]
    public int $number;

    #[ORM\Column]
    private int $space;

    #[ORM\Column]
    private bool $isReserved;

    #[ORM\ManyToOne(targetEntity: Parking::class, inversedBy: 'parkingPlaces')]
    private Parking $parking;

    #[ORM\OneToMany(mappedBy: 'parkingPlace', targetEntity: ParkingPlaceReservation::class)]
    private Collection $parkingPlaceReservations;

    public function __construct()
    {
        $this->parkingPlaceReservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;
        return $this;
    }

    public function getSpace(): ?int
    {
        return $this->space;
    }

    public function setSpace(int $space): self
    {
        $this->space = $space;
        return $this;
    }

    public function isIsReserved(): ?bool
    {
        return $this->isReserved;
    }

    public function setIsReserved(bool $isReserved): self
    {
        $this->isReserved = $isReserved;
        return $this;
    }

    public function getParking(): ?Parking
    {
        return $this->parking;
    }

    public function setParking(?Parking $parking): self
    {
        $this->parking = $parking;
        return $this;
    }

    /**
     * @return Collection<int, ParkingPlaceReservation>
     */
    public function getParkingPlaceReservations(): Collection
    {
        return $this->parkingPlaceReservations;
    }

    public function addParkingPlaceReservation(ParkingPlaceReservation $parkingPlaceReservation): self
    {
        if (!$this->parkingPlaceReservations->contains($parkingPlaceReservation)) {
            $this->parkingPlaceReservations[] = $parkingPlaceReservation;
            $parkingPlaceReservation->setParkingPlace($this);
        }
        return $this;
    }

    public function removeParkingPlaceReservation(ParkingPlaceReservation $parkingPlaceReservation): self
    {
        if ($this->parkingPlaceReservations->removeElement($parkingPlaceReservation)) {
            // set the owning side to null (unless already changed)
            if ($parkingPlaceReservation->getParkingPlace() === $this) {
                $parkingPlaceReservation->setParkingPlace(null);
            }
        }
        return $this;
    }
}
