<?php

namespace App\Entity\Medias;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Class\MediaImage;
use App\Entity\Event;
use App\Repository\Medias\MediaImageEventRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [new Get(), new Delete(), new GetCollection()], paginationEnabled: false)]
#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: MediaImageEventRepository::class)]
class MediaImageEvent extends MediaImage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;
    #[Vich\UploadableField(mapping: "media_image_event", fileNameProperty: "filePath")]
    #[Assert\NotNull]
    public ?File $file = null;
    #[ORM\ManyToOne(inversedBy: 'images', targetEntity: Event::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Event $event;
    #[ORM\Column]
    private DateTimeImmutable $updatedAt;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }
    public function setUpdatedAt(DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
    public function getEvent(): Event
    {
        return $this->event;
    }
    public function setEvent(?Event $event): self
    {
        $this->event = $event;
        return $this;
    }
}
