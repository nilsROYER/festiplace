<?php

namespace App\Api\Response;

use Symfony\Component\Serializer\Annotation\Groups;

class ForgottenPasswordResponse
{
    #[Groups(['read:user'])]
    public string $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }
}
