# **Cahier des charges Festiplace**

## **_Contexte_ :**

Festiplace est une plateforme d’échange pour les festivals et ses différent acteurs.
La plateforme met en relation des Festival, des stand divers et des festivalier.

Pour les festival elle permet une meilleur gestion de l’événement, espaces stands, parking, réservation de place , superficie utiliser, superficie libre, statistique. Ainsi qu’une présentation de l’événement et de son organisation.

Pour les stand elle permet de décrire son activité, de faire des demande de participation à un festival, connaitre les espaces libres dédier aux stands.

Pour un festivalier elle permet de se renseigner sur les différent festival sur la plateforme, permet de voir les place disponible, de faire des demande de réservation de place de parking.

## ***Les utilisateurs* :**

- Les organisateur de festival
- Les propriétaires de stand (professionnel ou amateur)
- Les festivaliers
- Les administrateurs

## ***Périmètre du projet* :**

- Gestion des utilisateurs et de leur permission en fonction de leur rôle (festival, stand, festivalier, admin)
- Gestions des événements en temps réel.
- Gestion des différente demande de réservation et d’adhésion.
- Outils de gestion pour les utilisateur si leur rôle leur permet (festival)
- Outils statistique pour les festivals et stands

## ***Hors Périmètre* :**

- Gestion de la billeterie

## ***Architecture de l’application* :**

- Une base de données Postgresql
- Une api REST en backend pour la gestion des données entrantes et sortantes (couche métier de l’application)
- Une interface web en vuejs (couche présentation de l’application coté web)
- Une application mobile (pour valider en temp réel des place de parking utiliser)

## ***Les livrables* :**

- Application web
- Application mobile
- API REST
- Documentation technique

## ***Fonctionnalités* :**

### ***Festival* :**

- Le festival peu donner une description de ses événement et de son organisation
- Le festival peu indiquer son nombre d’espace disponible pour les stand pour chacun de ses événement.
- Le festival peu indiquer la superficie d’un espace en m²
- Le festival peu indiquer le nombre de parking a leur disposition
- Le festival peu donner la superficie d’un parking en m².
- Le festival peu donner une catégorie aux parking (voiture, camion etc)
- Le festival peu donner une superficie minimum pour une place de parking (dans la limite du possible)
- Le festival peu indiquer les stand présent pendant l’événement
- Le festival peu accéder au portail web festival pour gérer c’est événement (back-office pour le festival)

### ***Stand* :**

- Un stand peu faire une description de son activité
- Un stand peu voir les espaces stand libre d’un festival
- Un stand peu faire une demande d’adhésion à un festival
- Sur un festival affilié un stand peu choisir sa zone d’emplacement
- Un stand peu avoir une catégorie
- Un stand peu accéder aux portail web stand (back-office pour les stand)
- Un stand peu indiquer sa recette du jour.

### **_Festivalier_ :**

- Un festivalier peu faire une demande de réservation d’une ou plusieurs places de parking
- Un festivalier peu voir la description d’un festival
- Un festivalier peu voir le nombre de parking et leurs types
- Un festivalier peu voir le nombre de place disponible d’un parking.

## ***Fonctionnalité Global* :**

- La plateforme met a disposition un back-office pour les festival pour gérer ses différent événement ,ajouter des espaces stand, ajouter des parkings, traiter les demandes d’adhésions des stands, traiter les demande de réservation de place (stand et parking), modifier ses informations.

- La plateforme met à disposition un back-office pour les propriétaire de stand pour gérer les différents événements auquel il est affilié, permet de faire des statistique sur les différent événement auquel il a participer, modifier ses informations.

- La plateforme met également une page de présentation pour les festivals et les stands pour leur apporter plus de visibilité.

- supprimer son compte.
- s’inscrire en tant qu’organisateur d’événement (Festival)
- s’inscrire en tant que propriétaire de stand (stand)
- s’inscrire en tant que festivalier
- voir la page de présentation d’un stand, d’un festival.
- faire une demande de réservation de place de parking.

# **_Interface web_ :**

## **_Page globals_ :**

- Page d'accueil présentation du site

- Page d'inscription création d'un compte festivalier, stand ou organisateur d'événement

- Page de connection du site

- Page festivals :

  - voir la liste des festivals présent sur la platforme
  - trier la liste des festivals,
  - rechercher un/des festivals présent sur la platforme
  - accéder à la page détail d'un festival

- Page stands trier, rechercher un/des stand(s) présent sur la platforme

- Page detail d'un festival permet de voir les infos d'un festival :

  - description de l'organisation,
  - liste des événements (passé, à venir),
  - ?email, ?numero de contact

- Page detail d'un événement permet de voir les infos d'un événement festif :

  - description,
  - coordonnées (addresse, point gps),
  - date de l'événement,
  - places disponibles,
  - espaces (stand, parking) plein/disponible
  - lien vers la billeterie
  - faire une demande de réservation de place de parking

  ### En tant que stand\*

  - Peut faire une requete d'adésion à un événement

- Page detail stand permet de voir les infos d'un stand :

  - description,
  - catégories,
  - événement affilié
  - ?email, ?numero contact

  ### En tant qu'organisateur d'événement\*

  - Peut faire une requéte d'adésion a l'événement

- Page événements :

  - Voir la liste des événements a venir
  - trier la liste des événements
  - rechercher un/des événement(s)
  - accéder à la page détail d'un événement

- Page contact permet de contacter les administrateur du site (moi)

## **_Page pour les organisateurs d'événement_ :**

- Page compte :

  - voir, modifier ses informations personnel
  - voir, modifier les informations de l'organisation
  - supprimer son compte

- Page back-office :
  - créer, modifier, supprimer un/des événement(s)
  - ajouter, modifier, suprimer un/des espaces stands, parkings pour un événement
  - traiter les demandes de réservation de place de parkings pour un événement
  - voir, modifier les information d'une place de stand
  - voir, modifier les informations d'une place de parking
  - traité les demandes d'adésion d'un stand à un événement
  - voir les stand affilié à un événement
  - avoir des statistique sur l'optimisation des espaces
  - voir, publier, supprimer des images

## **_Page pour les propriétaire de stands_ :**

- Page compte :

  - voir, modifier ses informations personnel
  - voir, modifier les informations d'un stand(s)
  - supprimer son compte

- Page back-office :
  - traiter les demande d'adésion d'un festival
  - voir les événement affilier (passer/à venir)
  - voir les espace de stand affilié
  - voir, publier, supprimer des images

## **_Page pour les administrateurs_ :**

- Page back-office :
  - créer, modifier, supprimer un compte (propriétaire de stand, organisateur d'événement, festivalier)
  - voir, modifier ses informations personnels
  - voir, modifier, supprimer les informations d'un stand(s)
  - voir, modifier, supprimer les informations d'une organisation
  - voir, modifier, supprimer un événement
  - voir, modifier, supprimer un espaces (stand ou parking)
  - voir, modifier, supprimer une place de stand
  - voir, modifier, une place de parking
  - voir, modifier, supprimer les demandes d'adésions de stand et de réservation de place de parkings

