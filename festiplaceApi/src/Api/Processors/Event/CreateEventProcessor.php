<?php

namespace App\Api\Processors\Event;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Event;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

final class CreateEventProcessor implements ProcessorInterface
{
    public function __construct(private readonly Security $security, private readonly PersistProcessor $persistProcessor)
    {
    }

    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $user = $this->security->getUser();
        if ($user instanceof User && $data instanceof Event) {
            $user->getOrganisation()->addEvent($data);
        }

        $this->persistProcessor->process($data, $operation, $uriVariables, $context);

        return $data;
    }
}
