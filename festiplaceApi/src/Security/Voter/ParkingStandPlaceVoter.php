<?php

namespace App\Security\Voter;

use App\Datas\Roles;
use App\Entity\ParkingStandPlace;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ParkingStandPlaceVoter extends Voter
{
    public const CREATE_PARKING_STAND_PLACE = 'CREATE_PARKING_STAND_PLACE';
    public const UPDATE_PARKING_STAND_PLACE = 'UPDATE_PARKING_STAND_PLACE';
    public const DELETE_PARKING_STAND_PLACE = 'DELETE_PARKING_STAND_PLACE';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {

        $supportAttributes = in_array($attribute, [self::CREATE_PARKING_STAND_PLACE, self::UPDATE_PARKING_STAND_PLACE, self::DELETE_PARKING_STAND_PLACE]);
        $supportSubject = $subject instanceof ParkingStandPlace;

        return $supportAttributes && $supportSubject;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User && $subject instanceof ParkingStandPlace) {
            return false;
        }

        return match ($attribute) {
            self::CREATE_PARKING_STAND_PLACE => $this->isOwnerOfParkingStand(subject: $subject, user: $user) || $this->security->isGranted(Roles::ROLE_OWNER_STAND) || $this->security->isGranted(Roles::ROLE_ADMIN),
            self::UPDATE_PARKING_STAND_PLACE => $this->isOwnerOfParkingStand(subject: $subject, user: $user) || $this->isOwnerOfStand(subject: $subject, user: $user) || $this->security->isGranted(Roles::ROLE_ADMIN),
            self::DELETE_PARKING_STAND_PLACE => $this->isOwnerOfParkingStand(subject: $subject, user: $user),
            default => false,
        };
    }

    private function isOwnerOfParkingStand(ParkingStandPlace $subject, User $user): bool
    {
        $isEventCreator = $this->security->isGranted(Roles::ROLE_EVENT_CREATOR->value);

        if ($isEventCreator) {
            $subjectParkingStand = $subject->getParkingStand();
            $userEvents = $user->getOrganisation()->getEvents();

            foreach ($userEvents as $event) {
                if ($event->getParkingStands()->contains($subjectParkingStand)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function isOwnerOfStand(ParkingStandPlace $subject, User $user): bool
    {
        $isOwnerStand = $this->security->isGranted(Roles::ROLE_OWNER_STAND->value);

        if ($isOwnerStand) {
            $subjectStand = $subject->getStand();
            $userStand = $user->getStand();

            return $subjectStand === $userStand;
        }

        return false;
    }
}
