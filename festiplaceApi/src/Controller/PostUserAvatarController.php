<?php

namespace App\Controller;

use App\Entity\Medias\MediaAvatar;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

#[AsController]
class PostUserAvatarController extends AbstractController
{
    public function __construct(
        private readonly Security $security
    ) {
    }
    public function __invoke(Request $request): User|UserInterface
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestException("file is required");
        }

        $user = $this->security->getUser(); // load user by jwt

        $avatar = $user->getAvatar();
        if (!$avatar instanceof MediaAvatar) { // if user have not avatar create it, else update it
            $avatar = new MediaAvatar();
        }

        $avatar->file = $uploadedFile;
        $avatar->setUpdatedAt(new \DateTimeImmutable()); // update date for dispatch vich event
        $user->setAvatar($avatar);

        return $user;
    }
}
