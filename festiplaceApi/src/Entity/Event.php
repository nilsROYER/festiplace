<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Api\Processors\Event\CreateEventProcessor;
use App\Controller\PostEventImageController;
use App\Entity\Medias\MediaImageEvent;
use App\Repository\EventRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new Put(security: 'is_granted(\'EVENT_UPDATE\', object'),
        new Delete(),
        new Post(
            security: 'is_granted(\'EVENT_UPLOAD_IMAGE\', object)',
            uriTemplate: '/events/{id}/image/upload',
            controller: PostEventImageController::class,
            deserialize: false
        ),
        new Post(
            securityPostDenormalize: 'is_granted(\'EVENT_CREATE\', object)',
            processor: CreateEventProcessor::class
        )],
    denormalizationContext: ['groups' => ['write:event']],
    normalizationContext: ['groups' => ['read:event']],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    #[Groups(['read:event'])]
    protected ?int $id;

    #[ORM\Column(length: 255, type: Types::STRING)]
    #[Groups(['read:event', 'write:event'])]
    protected string $name;

    #[ORM\Column(length: 255)]
    #[Groups(['read:event', 'write:event'])]
    private string $address;

    #[ORM\Column]
    #[Groups(['read:event', 'write:event'])]
    private int $maxPeaple;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['read:event', 'write:event'])]
    private string $description;

    #[ORM\Column]
    #[Groups(['read:event', 'write:event'])]
    private DateTimeImmutable $startAt;

    #[ORM\Column]
    #[Groups(['read:event', 'write:event'])]
    private DateTimeImmutable $endAt;

    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false, referencedColumnName: 'owner_id')]
    #[Groups(['read:event'])]
    private Organisation|int $organisation;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Parking::class, orphanRemoval: true)]
    #[Groups(['read:event'])]
    private Collection $parkings;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: ParkingStands::class, orphanRemoval: true)]
    #[Groups(['read:event'])]
    private Collection $parkingStands;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: MediaImageEvent::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:event'])]
    private Collection $images;

    public function __construct()
    {
        $this->parkings = new ArrayCollection();
        $this->parkingStands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }
    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }
    public function getMaxPeaple(): ?int
    {
        return $this->maxPeaple;
    }
    public function setMaxPeaple(int $maxPeaple): self
    {
        $this->maxPeaple = $maxPeaple;
        return $this;
    }
    public function getDescription(): string
    {
        return $this->description;
    }
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }
    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }
    public function setStartAt(\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;
        return $this;
    }
    public function getEndAt(): ?\DateTimeImmutable
    {
        return $this->endAt;
    }
    public function setEndAt(\DateTimeImmutable $endAt): self
    {
        $this->endAt = $endAt;
        return $this;
    }
    public function getOrganisation(): ?Organisation
    {
        return $this->organisation;
    }
    public function setOrganisation(?Organisation $organisation): self
    {
        $this->organisation = $organisation;
        return $this;
    }
    /**
     * @return Collection<int, Parking>
     */
    public function getParking(): Collection
    {
        return $this->parkings;
    }
    public function addParking(Parking $parking): self
    {
        if (!$this->parkings->contains($parking)) {
            $this->parkings[] = $parking;
            $parking->setEvent($this);
        }
        return $this;
    }
    public function removeParking(Parking $parking): self
    {
        if ($this->parkings->removeElement($parking)) {
            // set the owning side to null (unless already changed)
            if ($parking->getEvent() === $this) {
                $parking->setEvent(null);
            }
        }
        return $this;
    }
    /**
     * @return Collection<int, ParkingStands>
     */
    public function getParkingStands(): Collection
    {
        return $this->parkingStands;
    }
    public function addParkingStands(ParkingStands $parkingStand): self
    {
        if (!$this->parkingStands->contains($parkingStand)) {
            $this->parkingStands[] = $parkingStand;
            $parkingStand->setEvent($this);
        }
        return $this;
    }
    public function removeParkingStands(ParkingStands $parkingStand): self
    {
        if ($this->parkingStands->removeElement($parkingStand)) {
            if ($parkingStand->getEvent() === $this) {
                $parkingStand->setEvent(null);
            }
        }
        return $this;
    }
    /**
     * @return Collection<int, MediaImageEvent>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }
    public function addImage(MediaImageEvent $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setEvent($this);
        }
        return $this;
    }
    public function removeImage(MediaImageEvent $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getEvent() === $this) {
                $image->setEvent(null);
            }
        }
        return $this;
    }
}
