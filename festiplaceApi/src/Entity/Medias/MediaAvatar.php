<?php

namespace App\Entity\Medias;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Entity\User;
use DateTimeImmutable;
use App\Class\MediaImage;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Medias\MediaAvatarRepository;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [new Delete(), new Get(), new GetCollection()], paginationEnabled: false)]
#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: MediaAvatarRepository::class)]
class MediaAvatar extends MediaImage
{
    #[ORM\Id]
    #[ORM\OneToOne(inversedBy: 'avatar', cascade: ['persist']), ORM\JoinColumn(nullable: false)]
    private ?User $customer = null;
    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;
    #[Vich\UploadableField(mapping: "media_avatar", fileNameProperty: "filePath")]
    #[Assert\NotNull]
    public ?File $file = null;
    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;
    public function __construct()
    {
        $this->updatedAt = new DateTimeImmutable();
    }
    public function getId(): ?int
    {
        return $this->customer->getId();
    }
    public function getCustomer(): ?User
    {
        return $this->customer;
    }
    public function setCustomer(User $customer): self
    {
        $this->customer = $customer;
        return $this;
    }
    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }
    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
