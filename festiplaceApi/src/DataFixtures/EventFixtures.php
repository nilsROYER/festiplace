<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Repository\OrganisationRepository;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private OrganisationRepository $organisationRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $organisations = $this->organisationRepository->findAll();

        foreach ($organisations as $organisation) {
            $event = new Event();
            $event->setName(sprintf('%s édition 2022', $organisation->getName()))
                ->setAddress('dans un champ')
                ->setMaxPeaple(5000)
                ->setDescription('lorem ipsum')
                ->setStartAt(new DateTimeImmutable('2022-08-20'))
                ->setEndAt(new DateTimeImmutable('2022-08-26'));
            $organisation->addEvent($event);

            $manager->persist($event);
            $manager->persist($organisation);
        }

        $manager->flush();
    }

    public function getDependencies(): iterable
    {
        return [
            UserFixtures::class,
            OrganisationFixtures::class
        ];
    }
}
