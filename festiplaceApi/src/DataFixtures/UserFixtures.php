<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $hashPassword = $this->passwordHasher->hashPassword($user, sprintf('password%s', $i));
            $user->setEmail(sprintf('email%s@example.fr', $i))
                ->setPassword($hashPassword)
                ->setUsername(sprintf('user %s', $i))
                ->setFirstname(sprintf('firstname%s', $i))
                ->setLastname(sprintf('lastname%s', $i))
                ->setRoles(['ROLE_ADMIN', 'ROLE_STAND_OWNER', 'ROLE_EVENT_CREATOR'])
                ->setPhone('0637526382');

            $manager->persist($user);
        }

        $manager->flush();
    }
}
