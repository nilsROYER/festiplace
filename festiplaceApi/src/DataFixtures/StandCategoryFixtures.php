<?php

namespace App\DataFixtures;

use App\Entity\StandCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class StandCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 5; $i++) {
            $category = (new StandCategory())->setName(sprintf('category stand %s', $i));
            $manager->persist($category);
        }

        $manager->flush();
    }
}
