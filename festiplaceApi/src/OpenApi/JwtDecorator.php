<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

final class JwtDecorator implements OpenApiFactoryInterface
{
    const API_ROUTE = '/api/login';

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = new \ArrayObject(
            [
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
            ]
        );

        $schemas['Credentials'] = new \ArrayObject(
            [
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'exemple' => 'johndoe@exemple.com',
                ],
                'password' => [
                    'type' => 'string',
                    'exemple' => 'password123'
                ],
            ],
            ]
        );

        $pathItem = new Model\PathItem(
            ref: 'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItem',
                tags: ['Token'],
                responses: [
                    '200' => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Credentials',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get JWT token to login',
                requestBody: new Model\RequestBody(
                    description: 'Generate new JWT Token',
                    content: new \ArrayObject(
                        [
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials'
                            ],
                        ],
                        ]
                    ),
                ),
            ),
        );

        $openApi->getPaths()->addPath(self::API_ROUTE, $pathItem);

        return $openApi;
    }
}
