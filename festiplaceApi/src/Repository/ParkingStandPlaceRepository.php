<?php

namespace App\Repository;

use App\Entity\ParkingStandPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ParkingStandPlace>
 *
 * @method ParkingStandPlace|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParkingStandPlace|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParkingStandPlace[]    findAll()
 * @method ParkingStandPlace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParkingStandPlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParkingStandPlace::class);
    }

    public function add(ParkingStandPlace $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ParkingStandPlace $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ParkingStandPlace[] Returns an array of ParkingStandPlace objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ParkingStandPlace
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
