<?php

namespace App\Api\Request;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class ForgottenPasswordRequest
{
    #[Groups(['write:user'])]
    #[Assert\Email]
    public string $userEmail;

    private function __construct(string $email)
    {
        $this->userEmail = $email;
    }
}
