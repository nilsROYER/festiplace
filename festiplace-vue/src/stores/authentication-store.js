import { defineStore } from "pinia";
import { useHttpStore } from "./http-store";

export const useAuthenticationStore = defineStore('authentication', {
    state: () => ({
        // User information
        user: {
            id: null,
            username: null,
            firstname: null,
            lastname: null,
            phone: null,
            email: null,
            roles: [],
        },
        // Authentifications datas
        token: localStorage.getItem('token'),
        refresh: null,
        remember: false,

        // API base entrypoint
        BASE_URL: 'http://localhost:8000/api',

        // http store
        http: useHttpStore()
    }),

    actions: {
        disconnect() {
            console.log("auth store remove connection")
            localStorage.clear()
            this.$reset()
            this.$router.push('/login')
        },

        isConnected() {
            return this.token != null
        },

        async login(email, password, remember) {
            const reponse = await this.http.post(`${this.BASE_URL}/login`, { email: email, password: password })
            this.token = reponse.data.token
            this.remember = remember
            localStorage.setItem('token', this.token)
            localStorage.setItem('remember', this.remember)
            this.$router.push('/')
        },

        async createAccount(user) {
            const response = await this.http.post(`${this.BASE_URL}/users`, {
                email: user.email,
                username: user.username,
                firstname: user.firstname,
                lastname: user.lastname,
                phone: '0600000000',
                roles: [user.role],
                password: user.password
            })

            return response
        },

        async getUser() {
            const response = await this.http.get(`${this.BASE_URL}/user/profile`, {
                data: {},
                headers: { 'Authorization': `Bearer ${this.token}` }
            })
            const user = response.data
            console.log(user)
            this.user = {
                id: user.id,
                username: user.username,
                lastname: user.lastname,
                firstname: user.firstname,
                phone: user.phone,
                email: user.email,
                roles: user.roles
            }

            return response;
        },

        async updateProfile(newProfile) {
            console.log(newProfile)
            const response = await this.http.put(`${this.BASE_URL}/users/${this.user.id}`, newProfile)
            const user = response.data
            this.user = {
                id: user.id,
                username: user.username,
                lastname: user.lastname,
                firstname: user.firstname,
                phone: user.phone,
                email: user.email,
                roles: user.roles
            }
            return this.user
        },

        async resetPasswordRequest(email) {
            console.log(email)
            const response = await this.http.post(`${this.BASE_URL}/users/forgotten_password`, { userEmail: email })
            console.log(response)
        },

        async changePasswordRequest(token, password) {
            const response = await this.http.post(`${this.BASE_URL}/users/reset_password`, { resetToken: token, newPassword: password })
            console.log(response)
            if(response.status == 201) {
                localStorage.clear()
                this.$reset()
                this.$router.push('/login')
            }
        }
    }
})