<?php

namespace App\Api\Request;

use Symfony\Component\Serializer\Annotation\Groups;

class ResetPasswordRequest
{
    #[Groups(['write:user'])]
    public string $resetToken;

    #[Groups(['write:user'])]
    public string $newPassword;

    private function __construct(string $resetToken, string $newPassword)
    {
        $this->resetToken = $resetToken;
        $this->newPassword = $newPassword;
    }
}
