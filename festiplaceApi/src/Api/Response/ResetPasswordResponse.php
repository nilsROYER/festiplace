<?php

namespace App\Api\Response;

use Symfony\Component\Serializer\Annotation\Groups;

class ResetPasswordResponse
{
    #[Groups(['read:user'])]
    public string $message;

    public function __construct()
    {
        $this->message = "Password updated successfully";
    }
}
