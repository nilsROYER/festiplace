### Fonctionnalités

## Super Admin
    - Peut créer, modifier, supprimer des comptes.
    - Peut créer, modifier, supprimer des organisations.
    - Peut créer, modifier, supprimer des stands.
    - Peut bannir des utilisateur
    - Peut créer d'autre Administrateur

## Admin
    - Peut créer, modifier, supprimer des comptes.
    - Peut créer, modifier, supprimer des organisations.
    - Peut créer, modifier, supprimer des stands.
    - Peut bannir des utilisateur

## 