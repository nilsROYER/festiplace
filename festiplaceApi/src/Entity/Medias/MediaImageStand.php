<?php

namespace App\Entity\Medias;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Class\MediaImage;
use App\Entity\Stand;
use App\Repository\Medias\MediaImageStandRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [new Get(), new Delete(), new GetCollection()], paginationEnabled: false)]
#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: MediaImageStandRepository::class)]
class MediaImageStand extends MediaImage
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column]
    private ?int $id = null;
    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;
    #[Vich\UploadableField(mapping: "media_image_stand", fileNameProperty: "filePath")]
    #[Assert\NotNull]
    public ?File $file = null;
    #[ORM\ManyToOne(inversedBy: 'images', targetEntity: Stand::class)]
    #[ORM\JoinColumn(nullable: false, referencedColumnName: 'owner_id')]
    private Stand|int $stand;
    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;
    public function __construct()
    {
        $this->updatedAt = new DateTimeImmutable();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getStand(): ?Stand
    {
        return $this->stand;
    }
    public function setStand(?Stand $stand): self
    {
        $this->stand = $stand;
        return $this;
    }
    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }
    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
