<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Abstract\AbstractTestWithUser;

class UserApiTest extends AbstractTestWithUser
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request("GET", "/api/users");

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/User',
            '@id' => '/api/users',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => $response->toArray()['hydra:totalItems']
        ]);

        $this->assertCount(21, $response->toArray()['hydra:member']);
        $this->assertMatchesResourceCollectionJsonSchema(User::class);
    }

    public function testCreateUser(): void
    {
        $client = static::createClient();

        $response = $client->request('POST', '/api/users', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'test@test.fr',
                'username' => 'username',
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'password' => 'password',
                'phone' => '0700000000',
            ],
        ]);

        $json = $response->toArray();
        
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/User",
            "@type" => "User",
            "@id" => "/api/users/" . strval($json['id']),
            "id" => $json['id'],
            "email" => "test@test.fr",
            "username" => "username",
            "firstname" => "firstname",
            "lastname" => "lastname",
            "phone" => "0700000000",
            "roles" => [
                "ROLE_USER"
            ]
        ]);
    }

    public function testUpdateUser(): void
    {
        $iri = $this->findIriBy(User::class, ['id' => $this->getUser()->getId()]);

        $clientConnected = $this->createClientWithUserConnected();
        $clientConnected->request('PUT', $iri, ['json' => [
            "email" => 'update@email.fr',
            "password" => 'update password',
            "username" => "update username",
            "firstname" => 'update firstname',
            'lastname' => 'update lastname',
            'phone' => '0700000000',
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@id' => $iri,
            "id" => $this->getUser()->getId(),
            "email" => 'update@email.fr',
            "username" => "update username",
            "firstname" => 'update firstname',
            'lastname' => 'update lastname',
            "phone" => '0700000000',
            "roles" => [
                "ROLE_USER"
            ]
        ]);
    }

    public function testDeleteUser(): void
    {
        $iri = $this->findIriBy(User::class, ['id' => $this->getUser()->getId()]);

        $this->createClientWithUserConnected()->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(static::getContainer()->get(UserRepository::class)->findOneById($this->getUser()->getId()));
    }
}
