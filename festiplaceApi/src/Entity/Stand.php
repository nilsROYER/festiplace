<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Api\Processors\Stand\CreateStandProcessor;
use App\Controller\PostStandImageController;
use App\Entity\Medias\MediaImageStand;
use App\Repository\StandRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(),
        new Put(security: "is_granted('UPDATE_STAND', object)"),
        new Delete(security: "is_granted('DELETE_STAND', object)"),
        new Post(
            security: "is_granted('UPLOAD_IMAGE_STAND', object)",
            uriTemplate: '/stands/{owner}/image/upload',
            controller: PostStandImageController::class,
            deserialize: false
        ),

        new GetCollection(),
        new Post(
            securityPostDenormalize: "is_granted('CREATE_STAND', object)",
            securityPostDenormalizeMessage: 'Permition denied, You can\'t create another stand',
            processor: CreateStandProcessor::class
        )
    ],
    denormalizationContext: ['groups' => 'write:stand'],
    normalizationContext: ['groups' => 'read:stand'],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: StandRepository::class)]
class Stand
{
    #[ORM\Id]
    #[ORM\OneToOne(inversedBy: 'stand', targetEntity: User::class, cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:stand'])]
    private User|int $owner;

    #[ORM\Column(length: 255)]
    #[Groups(['write:stand', 'read:stand'])]
    private string $name;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['write:stand', 'read:stand'])]
    private string $description;

    #[ORM\Column]
    #[Groups(['write:stand', 'read:stand'])]
    private int $surface;

    #[ORM\Column]
    private DateTimeImmutable $createdAt;

    #[ORM\ManyToOne(targetEntity: StandCategory::class, inversedBy: 'stands')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['write:stand', 'read:stand'])]
    private StandCategory $category;

    #[ORM\OneToMany(mappedBy: 'stand', targetEntity: ParkingStandPlace::class)]
    #[Groups(['read:stand'])]
    private Collection $parkingStandPlaces;

    #[ORM\OneToMany(mappedBy: 'stand', targetEntity: MediaImageStand::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:stand'])]
    private Collection $images;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->parkingStandPlaces = new ArrayCollection();
        $this->images = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->owner->getId();
    }
    public function getName(): string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }
    public function getSurface(): ?int
    {
        return $this->surface;
    }
    public function setSurface(int $surface): self
    {
        $this->surface = $surface;
        return $this;
    }
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    public function getOwner(): ?User
    {
        return $this->owner;
    }
    public function setOwner(User|UserInterface $owner): self
    {
        $this->owner = $owner;
        return $this;
    }
    public function getCategory(): ?StandCategory
    {
        return $this->category;
    }
    public function setCategory(?StandCategory $category): self
    {
        $this->category = $category;
        return $this;
    }
    /**
     * @return Collection<int, ParkingStandPlace>
     */
    public function getParkingStandPlaces(): Collection
    {
        return $this->parkingStandPlaces;
    }
    public function addParkingStandPlace(ParkingStandPlace $parkingStandPlace): self
    {
        if (!$this->parkingStandPlaces->contains($parkingStandPlace)) {
            $this->parkingStandPlaces[] = $parkingStandPlace;
            $parkingStandPlace->setStand($this);
        }
        return $this;
    }
    public function removeParkingStandPlace(ParkingStandPlace $parkingStandPlace): self
    {
        if ($this->parkingStandPlaces->removeElement($parkingStandPlace)) {
            // set the owning side to null (unless already changed)
            if ($parkingStandPlace->getStand() === $this) {
                $parkingStandPlace->setStand(null);
            }
        }
        return $this;
    }
    /**
     * @return Collection<int, MediaImageStand>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }
    public function addImage(MediaImageStand $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setStand($this);
        }
        return $this;
    }
    public function removeImage(MediaImageStand $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getStand() === $this) {
                $image->setStand(null);
            }
        }
        return $this;
    }
}
