<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Medias\MediaImageEvent;
use App\Repository\EventRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class PostEventImageController extends AbstractController
{
    public function __construct(private readonly EventRepository $eventRepository)
    {
    }

    public function __invoke(Request $request): Event
    {

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestException("file is required");
        }

        $eventId = $request->attributes->get('id');
        $event = $this->eventRepository->findOneById($eventId);

        if (!$event instanceof Event) {
            throw new BadRequestException('Event not found, create it before');
        }

        $image = new MediaImageEvent();
        $image->file = $uploadedFile;
        $image->setUpdatedAt(new DateTimeImmutable());

        $event->addImage($image);

        return $event;
    }
}
