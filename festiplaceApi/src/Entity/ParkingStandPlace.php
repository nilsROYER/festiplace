<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\ParkingStandPlaceRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    operations: [
            new GetCollection(),
            new Post(securityPostDenormalize: "is_granted('CREATE_PARKING_STAND_PLACE', object)"),
            new Get(),
            new Put(security: "is_granted('UPDATE_PARKING_STAND_PLACE', object)"),
            new Delete(security: "is_granted('DELETE_PARKING_STAND_PLACE', object)"),
        ],
    paginationEnabled: false
)
]
#[ORM\Entity(repositoryClass: ParkingStandPlaceRepository::class)]
class ParkingStandPlace
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id;

    #[ORM\Column]
    private int $number;

    #[ORM\Column]
    private bool $isConfirmed;

    #[ORM\Column]
    private DateTimeImmutable $startAt;

    #[ORM\Column]
    private DateTimeImmutable $endAt;

    #[ORM\ManyToOne(targetEntity: ParkingStands::class, inversedBy: 'parkingStandPlaces')]
    private ParkingStands $parkingStand;

    #[ORM\ManyToOne(targetEntity: Stand::class, inversedBy: 'parkingStandPlaces')]
    #[ORM\JoinColumn(nullable: false, referencedColumnName: 'owner_id')]
    private Stand $stand;

    public function __construct()
    {
        $this->isConfirmed = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;
        return $this;
    }

    public function getIsConfirmed(): bool
    {
        return $this->isConfirmed;
    }

    public function setIsConfirmed(bool $isConfirmed): self
    {
        $this->isConfirmed = $isConfirmed;
        return $this;
    }

    public function getStartAt(): \DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;
        return $this;
    }

    public function getEndAt(): ?\DateTimeImmutable
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeImmutable $endAt): self
    {
        $this->endAt = $endAt;
        return $this;
    }

    public function getParkingStand(): ?ParkingStands
    {
        return $this->parkingStand;
    }

    public function setParkingStand(?ParkingStands $parkingStand): self
    {
        $this->parkingStand = $parkingStand;
        return $this;
    }

    public function getStand(): ?Stand
    {
        return $this->stand;
    }

    public function setStand(?Stand $stand): self
    {
        $this->stand = $stand;
        return $this;
    }
}
