<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Parking;
use App\Entity\ParkingCategory;
use App\Repository\EventRepository;
use App\Repository\ParkingCategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParkingFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private EventRepository $eventRepository, private ParkingCategoryRepository $parkingCategoryRepository)
    {
    }
    public function load(ObjectManager $manager): void
    {
        $events = $this->eventRepository->findAll();
        $parkingCategories = $this->parkingCategoryRepository->findAll();

        foreach ($events as $event) {
            for ($i = 0; $i < rand(0, 3); $i++) {
                $parking = (new Parking())
                    ->setTotalSpace(rand(100, 300));
                $parking->setFreeSpace($parking->getTotalSpace());

                $parkingCategory = $parkingCategories[rand(0, (count($parkingCategories) - 1))];
                $parkingCategory->addParking($parking);
                $event->addParking($parking);

                $manager->persist($parking);
                $manager->persist($event);
                $manager->persist($parkingCategory);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): iterable
    {
        return [
            EventFixtures::class,
            ParkingCategoryFixtures::class
        ];
    }
}
