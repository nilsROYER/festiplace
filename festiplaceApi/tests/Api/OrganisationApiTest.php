<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Organisation;
use App\Entity\User;
use App\Repository\OrganisationRepository;
use App\Tests\Abstract\AbstractTestWithAdmin;
use App\Tests\Abstract\AbstractTestWithEventCreator;

class OrganisationApiTest extends AbstractTestWithEventCreator
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request("GET", "/api/organisations");

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/Organisation',
            '@id' => '/api/organisations',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 20
        ]);

        $this->assertCount(20, $response->toArray()['hydra:member']);
        $this->assertMatchesResourceCollectionJsonSchema(Organisation::class);
    }

    public function testGetOrganisation(): void
    {
        $client = static::createClient();

        $response = $client->request('GET', '/api/organisations/18')->toArray();

        $iri = $this->findIriBy(Organisation::class, ['owner' => 18]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Organisation",
            "@type" => "Organisation",
            "@id" => $iri,
            "owner" => $response['owner'],
            "name" => $response['name'],
            "createdAt" => $response['createdAt'],
            "events" => $response['events'],
            "id" => $response['id']
        ]);
    }

    public function testCreateOrganisation(): void
    {
//        $client = static::createClient();
//
//        $response = $client->request('POST', '/api/users', [
//            'headers' => ['Content-Type' => 'application/json'],
//            'json' => [
//                'email' => 'test@test.fr',
//                'username' => 'username',
//                'firstname' => 'firstname',
//                'lastname' => 'lastname',
//                'password' => 'password',
//                'phone' => '0700000000',
//                'roles' => ['ROLE_USER', 'ROLE_EVENT_CREATOR']
//            ],
//        ]);

//        $json = $response->toArray();
        $iri = $this->findIriBy(User::class, ['id' => $this->getUser()->getId()]);

        $response = $this->createClientWithUserConnected()->request('POST', '/api/organisations', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'owner' => $iri,
                'name' => 'test create organisation'
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Organisation",
            "@type" => "Organisation",
            "@id" => "/api/organisations/" . strval($json['id']),
            "owner" => $iri,
            "name" => 'test create organisation',
            "createdAt" => $json['createdAt'],
            "events" => [],
            "id" => $json['id']
        ]);
    }

    public function testUpdateOrganisation(): void
    {
        $client = static::createClient();

        $response = $client->request('GET', '/api/organisations/18');
        $jsonResponse = $response->toArray();

        $iri = $this->findIriBy(Organisation::class, ['owner' => 18]);

        $this->createClientWithUserConnected()->request('PUT', $iri, ['json' => [
            "owner" => $jsonResponse['owner'],
            "name" => 'test update organisation'
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Organisation",
            "@type" => "Organisation",
            "@id" => "/api/organisations/" . strval($jsonResponse['id']),
            "owner" => $jsonResponse['owner'],
            "name" => 'test update organisation',
            "createdAt" => $jsonResponse['createdAt'],
            "events" => [],
            "id" => $jsonResponse['id']
        ]);
        
    }

    public function testDeleteOrganisation(): void
    {
        $iri = $this->findIriBy(Organisation::class, ['owner' => 10]);

        $this->createClientWithUserConnected()->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(static::getContainer()->get(OrganisationRepository::class)->findOneById(10));
    }
}