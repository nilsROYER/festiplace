import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/views/HomeView.vue'
import LoginPage from '@/views/LoginView.vue'
import NotfoundPage from '@/views/NotFoundView.vue'
import RegisterPage from '@/views/RegisterPage.vue'
import ProfilePage from '@/views/ProfileView.vue'
import Festivals from '@/views/FestivalsView.vue'
import Stands from '@/views/StandsView.vue'
import Events from '@/views/EventsView.vue'
import Contact from '@/views/ContactView.vue'
import ResetPasswordView from "@/views/ResetPasswordView.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },

    {
        path: '/profile',
        name: 'Profile',
        component: ProfilePage,
    },

    {
        path: '/profile/organisation',
        name: 'SeetingOrganisation',
        component: ProfilePage,
    },

    {
        path: '/profile/stand',
        name: 'SeetingStand',
        component: ProfilePage,
    },

    {
        path: '/festivals',
        name: 'Festivals',
        component: Festivals
    },

    {
        path: '/stands',
        name: 'Stands',
        component: Stands
    },

    {
        path: '/events',
        name: 'Events',
        component: Events
    },

    {
        path: '/contact',
        name: 'Contact',
        component: Contact
    },


    {
        path: '/register',
        name: 'Register',
        component: RegisterPage
    },

    {
        path: '/login',
        name: 'Login',
        component: LoginPage,
    },

    {
        path: '/reset_password',
        name: 'ResetPassword',
        component: ResetPasswordView
    },
    // will match everything and put it under `$route.params.pathMatch`
    { 
        path: '/:pathMatch(.*)*', 
        name: 'NotFound', 
        component: NotfoundPage
    },
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

export default router