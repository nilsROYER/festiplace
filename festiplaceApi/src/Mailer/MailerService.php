<?php

namespace App\Mailer;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService
{
    public function __construct(private readonly MailerInterface $mailer)
    {
    }

    public function send(Email $email): void
    {
        $this->mailer->send($email);
    }

    public static function GenerateTemplateEmail(
        User $user,
        string $html,
        string $subject = '',
        string $from = "festiplace@gmail.com",
        array $context = []
    ): Email {
        return (new TemplatedEmail())
            ->from($from)
            ->to($user->getEmail())
            ->subject($subject)
            ->htmlTemplate($html)
            ->context($context);
    }
}
