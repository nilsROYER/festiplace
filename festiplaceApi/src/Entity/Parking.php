<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ParkingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    operations: [
        new Get(),
        new Put(),
        new Delete(),
        new GetCollection(),
        new Post()
    ],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: ParkingRepository::class)]
class Parking
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column]
    private int $totalSpace;

    #[ORM\Column]
    private int $freeSpace;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'parkings')]
    #[ORM\JoinColumn(nullable: false)]
    private Event $event;

    #[ORM\ManyToOne(targetEntity: ParkingCategory::class, inversedBy: 'parkings')]
    #[ORM\JoinColumn(nullable: false)]
    private ParkingCategory $parkingCategory;

    #[ORM\OneToMany(mappedBy: 'parking', targetEntity: ParkingPlace::class, cascade: ['persist', 'remove'])]
    private Collection $parkingPlaces;

    public function __construct()
    {
        $this->parkingPlaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalSpace(): int
    {
        return $this->totalSpace;
    }

    public function setTotalSpace(int $totalSpace): self
    {
        $this->totalSpace = $totalSpace;
        return $this;
    }

    public function getFreeSpace(): int
    {
        return $this->freeSpace;
    }

    public function setFreeSpace(int $freeSpace): self
    {
        $this->freeSpace = $freeSpace;
        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;
        return $this;
    }

    public function getParkingCategory(): ?ParkingCategory
    {
        return $this->parkingCategory;
    }

    public function setParkingCategory(?ParkingCategory $parkingCategory): self
    {
        $this->parkingCategory = $parkingCategory;
        return $this;
    }

    /**
     * @return Collection<int, ParkingPlace>
     */
    public function getParkingPlaces(): Collection
    {
        return $this->parkingPlaces;
    }

    public function addParkingPlace(ParkingPlace $parkingPlace): self
    {
        if (!$this->parkingPlaces->contains($parkingPlace)) {
            $this->parkingPlaces[] = $parkingPlace;
            $parkingPlace->setParking($this);
        }
        return $this;
    }

    public function removeParkingPlace(ParkingPlace $parkingPlace): self
    {
        if ($this->parkingPlaces->removeElement($parkingPlace)) {
            // set the owning side to null (unless already changed)
            if ($parkingPlace->getParking() === $this) {
                $parkingPlace->setParking(null);
            }
        }
        return $this;
    }
}
