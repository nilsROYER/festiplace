<?php

namespace App\DataFixtures;

use App\Entity\Stand;
use App\Repository\StandCategoryRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class StandFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private StandCategoryRepository $standCategoryRepository
    ) {
    }
    public function load(ObjectManager $manager): void
    {
        $users = $this->userRepository->findAll();
        $category = $this->standCategoryRepository->findAll();
        foreach ($users as $user) {
            if (rand(0, 10) % 2) {
                $stand  = (new Stand())
                    ->setName(sprintf('stand user %s', $user->getId()))
                    ->setDescription('lorem ipsum')
                    ->setSurface(rand(5, 80))
                    ->setOwner($user);

                $cat = $category[rand(0, count($category) - 1)];
                $cat->addStand($stand);
                $manager->persist($stand);
                $manager->persist($cat);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): iterable
    {
        return [
            UserFixtures::class,
            StandCategoryFixtures::class
        ];
    }
}
