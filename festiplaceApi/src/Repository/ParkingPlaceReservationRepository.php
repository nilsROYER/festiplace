<?php

namespace App\Repository;

use App\Entity\ParkingPlaceReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ParkingPlaceReservation>
 *
 * @method ParkingPlaceReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParkingPlaceReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParkingPlaceReservation[]    findAll()
 * @method ParkingPlaceReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParkingPlaceReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParkingPlaceReservation::class);
    }

    public function add(ParkingPlaceReservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ParkingPlaceReservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ParkingPlaceReservation[] Returns an array of ParkingPlaceReservation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ParkingPlaceReservation
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
