## Contexte

### Objectifs de l'application
- L'objectif premier de l'application est de permettre a un festival de mieux optimiser ses espaces de parking, stand, scénes ou autre.

- Elle permet également de mettre en lien les organisateurs, les stand divers et les participant au festival.

### Les Utilisateurs
- Super admin
- Admin
- Organisateur d'événement
- Propriétaire de stand
- Participant

### Périmètre du project
- Gestion des utilisateurs
- Gestion des événement
- Statistique