<?php

namespace App\Security\Voter;

use App\Datas\Roles;
use App\Entity\ParkingStands;
use App\Entity\StandCategory;
use App\Entity\User;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ParkingStandsVoter extends Voter
{
    public const CREATE_PARKING_STAND = 'CREATE_PARKING_STAND';
    public const UPDATE_PARKING_STAND = 'UPDATE_PARKING_STAND';
    public const DELETE_PARKING_STAND = 'DELETE_PARKING_STAND';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        $supportAttributes = in_array($attribute, [self::CREATE_PARKING_STAND, self::UPDATE_PARKING_STAND, self::DELETE_PARKING_STAND]);
        $supportSubject = $subject instanceof ParkingStands;

        return $supportAttributes && $supportSubject;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof User && !$subject instanceof ParkingStands) {
            return false;
        }

        $hasEvent = $user->getOrganisation()->getEvents()->contains($subject->getEvent());
        return match ($attribute) {
            self::CREATE_PARKING_STAND,
            self::UPDATE_PARKING_STAND,
            self::DELETE_PARKING_STAND => ($this->security->isGranted(Roles::ROLE_EVENT_CREATOR->value) && $hasEvent),
            default => false
        };
    }
}
