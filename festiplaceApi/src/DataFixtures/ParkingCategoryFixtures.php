<?php

namespace App\DataFixtures;

use App\Entity\ParkingCategory;
use App\Entity\StandCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class ParkingCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 3; $i++) {
            $category = (new ParkingCategory())
                ->setName(sprintf("parking %s", $i))
                ->setSpaceMin(rand(3, 8));

            $manager->persist($category);
        }

        $manager->flush();
    }
}
