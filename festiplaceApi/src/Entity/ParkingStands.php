<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ParkingStandsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    operations: [
        new GetCollection(),
        new Post(securityPostDenormalize: "is_granted('CREATE_PARKING_STAND', object)"),
        new Get(),
        new Put(security: "is_granted('UPDATE_PARKING_STAND', object)"),
        new Delete(security: "is_granted('DELETE_PARKING_STAND', object)"),
    ],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: ParkingStandsRepository::class)]
class ParkingStands
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column]
    private int $totalSpace;

    #[ORM\Column]
    private int $freeSpace;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'parkingStands')]
    #[ORM\JoinColumn(nullable: false)]
    private Event $event;

    #[ORM\OneToMany(mappedBy: 'parkingStand', targetEntity: ParkingStandPlace::class)]
    private Collection $parkingStandPlaces;

    public function __construct()
    {
        $this->parkingStandPlaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalSpace(): int
    {
        return $this->totalSpace;
    }

    public function setTotalSpace(int $totalSpace): self
    {
        $this->totalSpace = $totalSpace;
        return $this;
    }

    public function getFreeSpace(): int
    {
        return $this->freeSpace;
    }

    public function setFreeSpace(int $freeSpace): self
    {
        $this->freeSpace = $freeSpace;
        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return Collection<int, ParkingStandPlace>
     */
    public function getParkingStandPlaces(): Collection
    {
        return $this->parkingStandPlaces;
    }

    public function addParkingStandPlace(ParkingStandPlace $parkingStandPlace): self
    {
        if (!$this->parkingStandPlaces->contains($parkingStandPlace)) {
            $this->parkingStandPlaces[] = $parkingStandPlace;
            $parkingStandPlace->setParkingStand($this);
        }
        return $this;
    }

    public function removeParkingStandPlace(ParkingStandPlace $parkingStandPlace): self
    {
        if ($this->parkingStandPlaces->removeElement($parkingStandPlace)) {
            // set the owning side to null (unless already changed)
            if ($parkingStandPlace->getParkingStand() === $this) {
                $parkingStandPlace->setParkingStand(null);
            }
        }
        return $this;
    }
}
