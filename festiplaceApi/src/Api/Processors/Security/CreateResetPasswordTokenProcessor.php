<?php

namespace App\Api\Processors\Security;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Doctrine\Common\State\RemoveProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Api\Request\ForgottenPasswordRequest;
use App\Api\Response\ForgottenPasswordResponse;
use App\Entity\PasswordToken;
use App\Entity\User;
use App\Exception\EmailNotFoundException;
use App\Mailer\MailerService;
use App\Repository\UserRepository;

final class CreateResetPasswordTokenProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly RemoveProcessor $removeProcessor,
        private readonly PersistProcessor $persistProcessor,
        private readonly MailerService $mailerService
    ) {
    }

    public function process($data, Operation $operation, array $uriVariables = [], array $context = []): ForgottenPasswordResponse
    {
        $user = null;
        // retrive the user
        if ($data instanceof ForgottenPasswordRequest) {
            $user = $this->userRepository->findOneBy(['email' => $data->userEmail]);
        }

        if (!$user instanceof User) {
            throw new EmailNotFoundException("No account has this email");
        }

        $resetPasswordToken = $user->getResetPasswordToken();
        if ($resetPasswordToken instanceof PasswordToken) {
            $this->removeProcessor->process($resetPasswordToken, $operation, $uriVariables, $context);
        }

        // create and set password token to change password in the second step
        $token = (new PasswordToken())
            ->setToken(md5(uniqid()));

        $user->setResetPasswordToken($token);
        $this->persistProcessor->process($user, $operation, $uriVariables, $context);

        // send email to user
        $this->sendEmail(user: $user, token: $token);

        return new ForgottenPasswordResponse(sprintf("An email as been sending to %s, open and read him to change your password", $user->getEmail()));
    }

    public function sendEmail(User $user, PasswordToken $token): void
    {
        $email = MailerService::GenerateTemplateEmail(user: $user, html: 'emails/forgotten_password_email.html.twig', subject: 'Change password request', context: ['token' => $token]);
        $this->mailerService->send($email); // envoie d'un email pour changer son mot de passe
    }
}
