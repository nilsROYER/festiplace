<?php

namespace App\Entity;

use App\Repository\PasswordTokenRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PasswordTokenRepository::class)]
class PasswordToken
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column()]
    #[Groups(['read:user'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:user'])]
    private string $token;

    #[ORM\Column]
    #[Groups(['read:user'])]
    private ?\DateTimeImmutable $expireAt = null;

    #[ORM\OneToOne(inversedBy: 'resetPasswordToken')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $owner = null;

    public function __construct()
    {
        $this->expireAt = new DateTimeImmutable('+1 hour');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpireAt(): ?\DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function setExpireAt(\DateTimeImmutable $expireAt): self
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function __toString(): string
    {
        return $this->token;
    }
}
