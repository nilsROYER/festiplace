<?php

namespace App\Api\Processors\Security;

use ApiPlatform\Doctrine\Common\State\RemoveProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Api\Request\ResetPasswordRequest;
use App\Api\Response\ResetPasswordResponse;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class ResetPasswordProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly HashPasswordProcessor $hashPasswordProcessor,
        private readonly RemoveProcessor $removeProcessor
    ) {
    }

    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // found the user by token if exist
        $user = null;
        if ($data instanceof ResetPasswordRequest) {
            $user = $this->userRepository->findUserByPasswordToken($data->resetToken);
        }

        if (!$user instanceof User) {
            throw new HttpException(message: "This token does not exist", statusCode: 404);
        }

        // set new password
        $user->setPassword($data->newPassword);
        $user = $this->hashPasswordProcessor->process($user, $operation, $uriVariables, $context); // hash the new password

        // remove the reset token
        $this->removeProcessor->process($user->getResetPasswordToken(), $operation, $uriVariables, $context);

        return new ResetPasswordResponse();
    }
}
