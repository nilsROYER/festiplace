<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\ParkingCategory;

class ParkingCategoryTest extends ApiTestCase
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', "api/parking_categories");

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/ParkingCategory',
            '@id' => '/api/parking_categories',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 3
        ]);
    }

    public function testCreateParkingCategory()
    {
        $client = static::createClient();
        $container = static::getContainer();

        $response = $client->request('POST', '/api/parking_categories', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => "new parking category",
                'spaceMin' => 5,
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/ParkingCategory",
            "@type" => "ParkingCategory",
            "@id" => "/api/parking_categories/" . strval($json['id']),
            "id" => $json['id'],
            "name" => "new parking category",
            "spaceMin" => 5,
            "parkings" => []
        ]);
    }

    public function testUpdateStandCategory()
    {
        $client = static::createClient();

        $iri = $this->findIriBy(ParkingCategory::class, ['id' => 1]);

        $response = $client->request('PUT', $iri, ['json' => [
            'id' => 1,
            "name" => "update parking category name",
            "spaceMin" => 7,
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@id' => $iri,
            "id" => 1,
            "name" => "update parking category name",
            "spaceMin" => 7,
            "parkings" => []
        ]);
    }

    public function testDeleteStandCategory(): void
    {
        $client = static::createClient();
        $container = static::getContainer();
        $iri = $this->findIriBy(ParkingCategory::class, ['id' => 1]);

        $client->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull($container->get('doctrine')
            ->getRepository(ParkingCategory::class)
            ->findOneBy(['id' => 1]));
    }
}