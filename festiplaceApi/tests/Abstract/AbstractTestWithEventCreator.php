<?php

namespace App\Tests\Abstract;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

abstract class AbstractTestWithEventCreator extends ApiTestCase
{
    private ?string $token = null;

    protected User $user;

    protected function getUser(): User
    {
        return $this->user;
    }

    public function setUp(): void
    {
        self::bootKernel();
        $user = (new User())
            ->setEmail('test02@example.com')
            ->setUsername('username')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setPhone('0600000000')
            ->setRoles(['ROLE_EVENT_CREATOR']);

        $user->setPassword(
            static::getContainer()->get(UserPasswordHasherInterface::class)->hashPassword($user, 'password')
        );

        $manager = static::getContainer()->get('doctrine')->getManager();
        $manager->persist($user);
        $manager->flush();

        $this->user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $user->getEmail()]);
    }

    protected function createClientWithUserConnected($token = null): Client
    {
        $token = $token ?? $this->getToken();

        return static::createClient([], ['headers' => ['authorization' => sprintf('Bearer %s', $token)]]);
    }

    protected function getToken($body = []): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/api/login', ['json' => $body ?: [
            'email' => 'test02@example.com',
            'password' => 'password',
        ]]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }
}