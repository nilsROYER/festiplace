<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class UserVoter extends Voter
{
    public const DELETE_USER = "DELETE_USER";

    public const UPDATE_USER = "UPDATE_USER";

    public function __construct(private Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        $supportAttributes = in_array($attribute, [self::DELETE_USER, self::UPDATE_USER]);
        $supportSubject = $subject instanceof User;

        return $supportAttributes && $supportSubject;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User */
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        // check conditions and return true to grant permission
        switch ($attribute) {
            case self::DELETE_USER:
                if (
                    ($this->security->isGranted('ROLE_USER') && $user->getId() == $subject->getId()) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
            case self::UPDATE_USER:
                if (
                    ($this->security->isGranted('ROLE_USER') && $user->getId() == $subject->getId()) ||
                    $this->security->isGranted('ROLE_ADMIN')
                ) {
                    return true;
                }
                break;
        }

        return false;
    }
}
