import { createApp, markRaw } from 'vue'
import { createPinia } from 'pinia'
import router from './router/router.js'
import App from './App.vue'
import './index.css'
/* FONTAWESOME ICONS COMPONENT */
import { library } from '@fortawesome/fontawesome-svg-core' // import the fontawesome core 
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome' // import font awesome icon component
import { faUserSecret, faUser } from '@fortawesome/free-solid-svg-icons' // import specific icons
import { faUsers } from '@fortawesome/free-solid-svg-icons'
import { faStore } from '@fortawesome/free-solid-svg-icons'
import { faHouse } from '@fortawesome/free-solid-svg-icons'
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons'
import { faAddressCard } from '@fortawesome/free-solid-svg-icons'
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons'
import { faXmark } from '@fortawesome/free-solid-svg-icons'
import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
/* add icons to the librarie */
library.add(
    faUserSecret,
    faUser,
    faUsers,
    faStore,
    faHouse,
    faCalendarCheck,
    faAddressCard,
    faCircleExclamation,
    faXmark,
    faArrowsRotate,
    faMagnifyingGlass
)

/* pinia config (stores) */
const pinia = createPinia()
    .use(({store}) => {
        store.$router = markRaw(router)
    })

/* App config */
createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(pinia)
    .use(router)
    .mount('#app')
