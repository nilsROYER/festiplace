<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Api\Processors\Security\CreateResetPasswordTokenProcessor;
use App\Api\Processors\Security\HashPasswordProcessor;
use App\Api\Processors\Security\ResetPasswordProcessor;
use App\Api\Request\ForgottenPasswordRequest;
use App\Api\Request\ResetPasswordRequest;
use App\Api\Response\ForgottenPasswordResponse;
use App\Api\Response\ResetPasswordResponse;
use App\Controller\GetProfileController;
use App\Controller\PostUserAvatarController;
use App\Entity\Medias\MediaAvatar;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Get(),
        new Put(
            security: "is_granted('UPDATE_USER', object)",
            denormalizationContext: ['groups' => ['update:user']],
        ),
        new Delete(security: "is_granted('DELETE_USER', object)"),
        new GetCollection(),
        new Post(processor: HashPasswordProcessor::class),
        new GetCollection(
            uriTemplate: '/user/profile',
            controller: GetProfileController::class,
            security: 'is_granted(\'ROLE_USER\')',
        ),
        new Post(
            security: 'is_granted(\'ROLE_USER\')',
            uriTemplate: '/users/profile/avatar',
            controller: PostUserAvatarController::class,
            deserialize: false
        ),
        new Post(
            uriTemplate: 'users/forgotten_password',
            input: ForgottenPasswordRequest::class,
            output: ForgottenPasswordResponse::class,
            processor: CreateResetPasswordTokenProcessor::class
        ),
        new Post(
            uriTemplate: 'users/reset_password',
            input: ResetPasswordRequest::class,
            output: ResetPasswordResponse::class,
            processor: ResetPasswordProcessor::class
        )
    ],
    denormalizationContext: ['groups' => ['write:user']],
    normalizationContext: ['groups' => ['read:user']],
    paginationEnabled: false
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity('email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    #[Groups(['read:user'])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Assert\Email]
    #[Groups(['read:user', 'write:user', 'update:user'])]
    private string $email;

    #[ORM\Column(length: 60)]
    #[Groups(['read:user', 'write:user', 'update:user'])]
    private string $username;

    #[ORM\Column(length: 60)]
    #[Groups(['read:user', 'write:user', 'update:user'])]
    private string $firstname;

    #[ORM\Column(length: 60)]
    #[Groups(['read:user', 'write:user', 'update:user'])]
    private string $lastname;

    #[ORM\Column(length: 20)]
    #[Groups(['read:user', 'write:user', 'update:user'])]
    private string $phone;

    #[ORM\Column(type: 'json')]
    #[Groups(['read:user', 'write:user'])]
    private array $roles = [];

    #[ORM\Column]
    #[Groups(['write:user', 'read:user'])]
    private string $password;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[ORM\OneToOne(mappedBy: 'owner', targetEntity: Organisation::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:user'])]
    private ?Organisation $organisation = null;

    #[ORM\OneToOne(mappedBy: 'owner', targetEntity: Stand::class, cascade: ['persist', 'remove'])]
    #[Groups(['read:user'])]
    private ?Stand $stand = null;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: ParkingPlaceReservation::class)]
    private Collection $parkingPlaceReservations;

    #[ORM\OneToOne(mappedBy: 'customer', cascade: ['persist', 'remove'])]
    #[Groups(['read:user'])]
    private ?MediaAvatar $avatar = null;

    #[ORM\OneToOne(mappedBy: 'owner', cascade: ['persist', 'remove'])]
    private ?PasswordToken $resetPasswordToken = null;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->parkingPlaceReservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getlastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getOrganisation(): ?Organisation
    {
        return $this->organisation;
    }

    public function setOrganisation(Organisation $organisation): self
    {
        if ($organisation->getOwner() !== $this) {
            $organisation->setOwner($this);
        }
        $this->organisation = $organisation;
        return $this;
    }

    public function getStand(): ?Stand
    {
        return $this->stand;
    }

    public function setStand(Stand $stand): self
    {
        if ($stand->getOwner() !== $this) {
            $stand->setOwner($this);
        }
        $this->stand = $stand;
        return $this;
    }

    /**
     * @return Collection<int, ParkingPlaceReservation>
     */
    public function getParkingPlaceReservations(): Collection
    {
        return $this->parkingPlaceReservations;
    }

    public function addParkingPlaceReservation(ParkingPlaceReservation $parkingPlaceReservation): self
    {
        if (!$this->parkingPlaceReservations->contains($parkingPlaceReservation)) {
            $this->parkingPlaceReservations[] = $parkingPlaceReservation;
            $parkingPlaceReservation->setOwner($this);
        }
        return $this;
    }

    public function removeParkingPlaceReservation(ParkingPlaceReservation $parkingPlaceReservation): self
    {
        if ($this->parkingPlaceReservations->removeElement($parkingPlaceReservation)) {
            // set the owning side to null (unless already changed)
            if ($parkingPlaceReservation->getOwner() === $this) {
                $parkingPlaceReservation->setOwner(null);
            }
        }
        return $this;
    }

    public function getAvatar(): ?MediaAvatar
    {
        return $this->avatar;
    }

    public function setAvatar(MediaAvatar $avatar): self
    {
        // set the owning side of the relation if necessary
        if ($avatar->getCustomer() !== $this) {
            $avatar->setCustomer($this);
        }
        $this->avatar = $avatar;
        return $this;
    }

    public function getResetPasswordToken(): ?PasswordToken
    {
        return $this->resetPasswordToken;
    }

    public function setResetPasswordToken(PasswordToken $resetPasswordToken): self
    {
        // set the owning side of the relation if necessary
        if ($resetPasswordToken->getOwner() !== $this) {
            $resetPasswordToken->setOwner($this);
        }
        $this->resetPasswordToken = $resetPasswordToken;
        return $this;
    }
}
