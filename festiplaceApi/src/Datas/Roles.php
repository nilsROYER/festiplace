<?php

namespace App\Datas;

enum Roles: string
{
    case ROLE_USER = "ROLE_USER";
    case ROLE_OWNER_STAND = "ROLE_OWNER_STAND";
    case ROLE_EVENT_CREATOR = "ROLE_EVENT_CREATOR";
    case ROLE_ADMIN = "ROLE_ADMIN";
    case PUBLIC_ACCESS = "PUBLIC_ACCESS";
}
