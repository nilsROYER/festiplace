import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import { expect } from 'vitest'
import { test } from 'vitest'
import HomeView from '../../src/views/HomeView.vue'

test('mount component HomeView', async () => {
    expect(HomeView).toBeTruthy()

    const wrapper = mount(HomeView, {
        global: {
            plugins: [createTestingPinia({ stubActions: false })]
        }
    })


    expect(wrapper.text()).toContain("Hello world!")
})