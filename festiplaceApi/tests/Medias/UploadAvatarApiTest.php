<?php

namespace App\Tests\Medias;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use App\Tests\Abstract\AbstractTestWithUser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

class UploadAvatarApiTest extends AbstractTestWithUser
{
    public function testUserUploadAvatar(): void
    {
        // copy de notre fichier de test pour eviter de replacer un ficher a chaque fois
        $filesystem = new Filesystem();
        $filesystem->copy(__DIR__ . '/../fixtures/static_test.jpg', __DIR__ . '/../fixtures/test.jpg');

        $file = new UploadedFile(__DIR__ . '/../fixtures/test.jpg', 'test.jpg');
        $response  = $this->createClientWithUserConnected()->request('POST', '/api/users/profile/avatar', [
            'headers' => [
                'content-type' => 'multipart/form-data'
            ],
            'extra' => [
                'files' => [
                    'file' => $file
                ],
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(User::class);

        $this->assertJsonContains([
            "@context" => "/api/contexts/User",
            "@type" => "User",
            "@id" => "/api/users/" . strval($json['id']),
            "id" => $json['id'],
            "email" => "test@example.com",
            "username" => "username",
            "firstname" => "firstname",
            "lastname" => "lastname",
            "phone" => "0600000000",
            "roles" => [
                "ROLE_USER"
            ],
            'avatar' => $json['avatar']
        ]);

        // $file->move(__DIR__ . '/../fixtures/', 'test.jpg');
    }
}
