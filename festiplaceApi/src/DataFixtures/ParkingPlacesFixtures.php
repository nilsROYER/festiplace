<?php

namespace App\DataFixtures;

use App\Entity\Parking;
use App\Entity\ParkingPlace;
use App\Repository\ParkingRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Config\Framework\Workflows\WorkflowsConfig\PlaceConfig;

class ParkingPlacesFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private ParkingRepository $parkingRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $parkings = $this->parkingRepository->findAll();

        foreach ($parkings as $parking) {
            $placeSpace = $parking->getParkingCategory()->getSpaceMin() + rand(0, 4);
            $totalPlaces = $parking->getTotalSpace() / $placeSpace;
            for ($i = 1; $i <= $totalPlaces; $i++) {
                $parkingPlace = (new ParkingPlace())
                    ->setNumber($i)
                    ->setSpace($placeSpace)
                    ->setIsReserved(false);

                $parking->addParkingPlace($parkingPlace);
                // $manager->persist($parkingPlace);
                $manager->persist($parking);
            }

            $manager->flush();
        }
    }

    public function getDependencies(): iterable
    {
        return [
            ParkingFixtures::class
        ];
    }
}
