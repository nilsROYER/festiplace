<?php

namespace App\Controller;

use App\Entity\Medias\MediaImageStand;
use App\Entity\Stand;
use App\Entity\User;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Security\Core\Security;

#[AsController]
final class PostStandImageController extends AbstractController
{
    public function __construct(private readonly Security $security)
    {
    }

    public function __invoke(Request $request): Stand
    {
        $uploadedFile = $request->files->get('file');

        if (!$uploadedFile) {
            throw new BadRequestException("file is required");
        }

        /** @var User */
        $user = $this->security->getUser();
        /** @var Stand */
        $stand = $user->getStand();

        if (!$stand instanceof Stand) {
            throw new BadRequestException('Stand is required, create it before');
        }

        $image = new MediaImageStand();
        $image->file = $uploadedFile;
        $image->setUpdatedAt(new DateTimeImmutable());

        $stand->addImage($image);

        return $stand;
    }
}
