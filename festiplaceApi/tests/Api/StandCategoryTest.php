<?php

namespace App\Tests\Api;

use App\Entity\StandCategory;
use App\Repository\StandCategoryRepository;
use App\Tests\Abstract\AbstractTestWithAdmin;

class StandCategoryTest extends AbstractTestWithAdmin
{
    public function testGetCollection(): void
    {
        static::createClient()->request('GET', "api/stand_categories");

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/StandCategory',
            '@id' => '/api/stand_categories',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 5
        ]);
    }

    public function testCreateStandCategory()
    {
        $response = $this->createClientWithUserConnected()->request('POST', '/api/stand_categories', [
            'json' => [
                'name' => "new stand category"
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/StandCategory",
            "@type" => "StandCategory",
            "@id" => "/api/stand_categories/" . strval($json['id']),
            "id" => $json['id'],
            "name" => "new stand category",
            "stands" => []
        ]);
    }

    public function testUpdateStandCategory()
    {
        $iri = $this->findIriBy(StandCategory::class, ['id' => 1]);

        $response = $this->createClientWithUserConnected()->request('PUT', $iri, ['json' => [
            'id' => 1,
            "name" => "update stand category name",
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@id' => $iri,
            "id" => 1,
            "name" => "update stand category name",
            "stands" => $response->toArray()['stands']
        ]);
    }

    public function testDeleteStandCategory(): void
    {
        $iri = $this->findIriBy(StandCategory::class, ['id' => 1]);

        $this->createClientWithUserConnected()->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(static::getContainer()->get(StandCategoryRepository::class)->findOneBy(['id' => 1]));
    }
}