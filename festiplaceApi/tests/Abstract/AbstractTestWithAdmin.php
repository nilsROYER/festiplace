<?php

namespace App\Tests\Abstract;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

abstract class AbstractTestWithAdmin extends ApiTestCase
{
    private ?string $token = null;

    protected User $user;

    protected function getUser(): User
    {
        return $this->user;
    }

public function setUp(): void
{
    self::bootKernel();
    $this->user = (new User())
    ->setEmail('test03@example.com')
    ->setUsername('username')
    ->setFirstname('firstname')
    ->setLastname('lastname')
    ->setPhone('0600000000')
    ->setRoles(['ROLE_ADMIN']);

    $this->user->setPassword(
            static::getContainer()->get(UserPasswordHasherInterface::class)->hashPassword($this->user, 'password')
    );

    $manager = static::getContainer()->get('doctrine')->getManager();
    $manager->persist($this->user);
    $manager->flush();
}

    protected function createClientWithUserConnected($token = null): Client
    {
        $token = $token ?? $this->getToken();

        return static::createClient([], ['headers' => ['authorization' => sprintf('Bearer %s', $token)]]);
    }

    protected function getToken($body = []): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/api/login', ['json' => $body ?: [
            'email' => 'test03@example.com',
            'password' => 'password',
        ]]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }
}