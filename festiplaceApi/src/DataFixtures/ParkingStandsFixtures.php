<?php

namespace App\DataFixtures;

use App\Entity\ParkingStands;
use App\Repository\EventRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParkingStandsFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private EventRepository $eventRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $events = $this->eventRepository->findAll();

        foreach ($events as $event) {
            for ($i = 0; $i <= rand(0, 4); $i++) {
                $parkingStand = (new ParkingStands())
                    ->setTotalSpace(rand(100, 1000));

                $parkingStand->setFreeSpace($parkingStand->getTotalSpace());
                $event->addParkingStands($parkingStand);

                $manager->persist($parkingStand);
                $manager->persist($event);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): iterable
    {
        return [
            EventFixtures::class
        ];
    }
}
