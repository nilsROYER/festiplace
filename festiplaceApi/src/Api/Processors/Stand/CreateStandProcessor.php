<?php

namespace App\Api\Processors\Stand;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Stand;
use Symfony\Component\Security\Core\Security;

final class CreateStandProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly PersistProcessor $persistProcessor,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $user = $this->security->getUser();
        if ($data instanceof Stand) {
            $data->setOwner($user);
        }

        $this->persistProcessor->process($data, $operation, $uriVariables, $context);

        return $data;
    }
}
