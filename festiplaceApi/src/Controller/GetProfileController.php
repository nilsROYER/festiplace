<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Security\Core\User\UserInterface;

#[AsController]
class GetProfileController extends AbstractController
{
    public function __construct(private readonly Security $security)
    {
    }

    public function __invoke(): User|UserInterface
    {
        return $this->security->getUser();
    }
}
