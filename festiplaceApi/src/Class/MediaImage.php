<?php

/*
 *
 * Nils Royer <nlsroyer@gmail.com>
 *
 * class for manage normalization and not persisted datas
 */
namespace App\Class;

use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;

#[Vich\Uploadable]
abstract class MediaImage
{
    // #[Groups(['read:user'])]
    public ?string $contentUrl = null;

    abstract protected function getId();
}
