<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Stand;
use App\Entity\StandCategory;
use App\Entity\User;
use App\Repository\StandRepository;
use App\Tests\Abstract\AbstractTestWithStandOwner;

class StandApiTest extends AbstractTestWithStandOwner
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request("GET", "/api/stands")->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
                '@context' => '/api/contexts/Stand',
                '@id' => '/api/stands',
                '@type' => 'hydra:Collection',
                'hydra:totalItems' => $response['hydra:totalItems']
        ]);

        $this->assertMatchesResourceCollectionJsonSchema(Stand::class);
    }

    public function testCreateUpdateAndDeleteStand(): void
    {
        // retrieve a token
        $token = $this->getToken();
        // iri for add stand to a categorie
        $categorieStandIri = $this->findIriBy(StandCategory::class, ['id' => 1]);

        // create stand
        $clientConnected = $this->createClientWithUserConnected($token);
        $response = $clientConnected->request('POST', '/api/stands', [
            'headers' => [
                'content-type' => 'application/json',
            ],
            'json' => [
                'name' => 'stand name',
                'description' => 'stand description',
                'surface' => 10,
                'category' => $categorieStandIri
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Stand",
            "@type" => "Stand",
            "@id" => $json['@id'],
            "owner" => $json['owner'],
            "name" => 'stand name',
            "description" => 'stand description',
            "surface" => 10,
            'category' => $categorieStandIri,
            "parkingStandPlaces" => [],
            "images" => []
        ]);

        $standIri = $json['@id'];

        $response = $clientConnected->request('PUT', $standIri, [
            'headers' => [
                'content-type' => 'application/json'
            ],
            'json' => [
                'name' => 'stand name update',
                'description' => 'stand description update',
                'surface' => 10,
                'category' => $json['category']
            ],
        ]);

        $json = $response->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            "@context" => "/api/contexts/Stand",
            "@type" => "Stand",
            "@id" => $json['@id'],
            "owner" => $json['owner'],
            "name" => 'stand name update',
            "description" => 'stand description update',
            "surface" => 10,
            'category' => $categorieStandIri,
            "parkingStandPlaces" => [],
            "images" => []
        ]);

        $clientConnected->request('DELETE', $standIri, [
            'headers' => [
                'content-type' => 'application/json'
            ],
        ]);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(static::getContainer()->get(StandRepository::class)->findOneBy(['owner' => $this->getUser()->getId()]));
    }
}
